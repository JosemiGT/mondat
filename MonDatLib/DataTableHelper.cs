﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace MonDatLib
{
    
    public class DataTableHelper
    {
        public bool CompareHeader(DataTable data1, DataTable data2)
        {
            bool isEqual = false;

            if(CheckDataTable(data1) && 
               CheckDataTable(data2) &&
               data1.Columns.Count == data2.Columns.Count)
            {               
                List<string> header1 = new List<string>();
                List<string> header2 = new List<string>();

                foreach (string head in data1.Columns) header1.Add(head);
                foreach (string head in data2.Columns) header2.Add(head);

                EqualityComparer<string> comparer = EqualityComparer<string>.Default;

                isEqual = header1.SequenceEqual(header2);
            }

            return isEqual;
        }

        public bool CheckDataTable(DataTable data)
        {
            return (data != null && data.Columns.Count > 0);
        }
    }
}
