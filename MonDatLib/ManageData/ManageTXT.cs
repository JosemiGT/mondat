//TODO.
using System.Data;
using System.IO;

namespace MonDatLib.ManageFiles
{
    class ManageTXT : IManageData
    {
        private readonly string delimiter;

        public ManageTXT(string delimiter)
        {
            this.delimiter = delimiter;
        }
        public DataTable ReadData(string path, string query)
        {
            throw new System.NotImplementedException();
        }

        public bool UpdateData(DataTable data, string outPath)
        {
            throw new System.NotImplementedException();
        }

        public bool WriteData(DataTable data, string outPath)
        {
            bool isWrite = false;

            if(data != null && data.Columns.Count > 0 && !string.IsNullOrWhiteSpace(outPath))
            {
               using(StreamWriter writer = new StreamWriter(outPath, false))
               {
                   string header = string.Empty;

                   foreach(string head in data.Columns)
                   {
                       header += head + delimiter;
                   }
                   writer.Write(header);

                   foreach(DataRow dataRow in data.Rows)
                   {
                       string row = string.Empty;

                       foreach(object item in dataRow.ItemArray)
                       {
                           row += item.ToString() + delimiter;
                       }

                       writer.Write(row);
                   }

                   isWrite = true;
               }
            }

            return isWrite;
        }
    }
}