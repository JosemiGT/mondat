﻿
using System.Data;

namespace MonDatLib.ManageFiles
{
    public interface IManageData
    {
        DataTable ReadData(string path, string query = "");
        bool WriteData(DataTable data, string outPath);
        bool UpdateData(DataTable data, string outPath);
    } 

}