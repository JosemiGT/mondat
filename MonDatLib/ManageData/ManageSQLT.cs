using System.Data;
using Microsoft.Data.SqlClient;

namespace MonDatLib.ManageFiles
{
    public class ManageSQLT : IManageData
    {
        public DataTable ReadData(string connectionString, string queryString)
        {
            DataTable data = new DataTable();

            if(!string.IsNullOrWhiteSpace(connectionString) && !string.IsNullOrWhiteSpace(queryString))
            {
                using(SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = new SqlCommand(queryString))
                    {
                        connection.Open();
                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            data.Load(reader);
                        }
                    }
                }
            }

            return data;
        }

        public bool UpdateData(DataTable data, string outPath)
        {
            throw new System.NotImplementedException();
        }

        public bool WriteData(DataTable data, string connectionString)
        {
            throw new System.NotImplementedException();
        }
    }
}